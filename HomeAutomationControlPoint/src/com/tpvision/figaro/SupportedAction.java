/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.tpvision.figaro.ActionParameter.ParameterIdEnum;

@Root(name="Command")
public class SupportedAction 
{
	public enum HA_ActionTypeEnum
	{	
		ON,
		OFF,
		TOGGLE,
		DIMMING,
		DIMMING_START,
		DIMMING_DONE,
		BIND,
		UNBIND,
		CHANGE_NAME,
		SUBSCRIBE_EVENTS,
		UNSUBSCRIBE_EVENTS,
		VALUE_CHANGED,
		NONE
	};
	
	@Attribute(name="name", required=false)
	String payloadName;
	
	@Attribute(name="format", required=false)
	String payloadFormat;
		
	@Attribute(name="optional", required=false)
	Boolean isPayloadOptional;
	
	@ElementList(name="Parameters", required=false)
	List<ActionParameter> parameters = new ArrayList<ActionParameter>();
	
	public SupportedAction()
	{
		this.isPayloadOptional = true;
		this.payloadFormat="";
		this.payloadName="";
	}
	
	public HA_ActionTypeEnum getActionType() 
	{
		return HA_ActionTypeEnum.valueOf(payloadName);
	}
	
	public void setCurrentValue(ParameterIdEnum parameter, double value)
	{
		try
		{			
			for (int j=0; j<parameters.size(); j++)
			{
				if (parameters.get(j).id == parameter)
				{
					parameters.get(j).currentValue = value;
				}
			}		
		}catch(Exception e)
		{}
	}
	
	public double getCurrentValue(ParameterIdEnum parameter)
	{
		try
		{			
			for (int j=0; j<parameters.size(); j++)
			{
				if (parameters.get(j).id == parameter)
				{					
					return Double.parseDouble(String.valueOf(parameters.get(j).currentValue));
				}
			}
		}catch(Exception e)
		{}
		return -1;
	}
	
	public String getFormatedPayload()
	{
		String payload="";
		try
		{
			if (!this.payloadFormat.equals(""))
			{			
				String[] parameters = this.payloadFormat.replace(".", "").split(",");
			
				ParameterIdEnum parameter;
				for (int i=0; i<parameters.length;i++)
				{			
					parameter = ParameterIdEnum.valueOf(parameters[i]);		
					payload += String.valueOf((int)getCurrentValue(parameter));
					if (i!=parameters.length-1)
					{
						payload += ",";	
					}
				}
			}
		}catch(Exception e)
		{		
			payload="";
		}
		payload += ".";
		return payload;
	}

	public static String getFormatedPayload(List<SupportedAction> supportedActions, HA_ActionTypeEnum actionType)
	{
		for (SupportedAction supportedAction: supportedActions)
		{
			if (supportedAction.getActionType() == actionType)
			{
				return supportedAction.getFormatedPayload();
			}
		}
		return ".";
	}
}

/*
 <Command name="DIMMING" format="R,G,B,T." optional="1"> 
	<Parameters>
		<Parameter id="R" name="Red" type="xs:unsignedShort" minvalue="0" maxvalue="4094"/>
		<Parameter id="G" name="Green" type="xs:unsignedShort" minvalue="0" maxvalue="4094"/>
		<Parameter id="B" name="Blue" type="xs:unsignedShort" minValue="0" maxValue="4094"/>
		<Parameter id="T" name="Time" type="xs:unsignedShort" minvalue="0" maxvalue="65535"/>
	</Parameters>
</Command>
*/