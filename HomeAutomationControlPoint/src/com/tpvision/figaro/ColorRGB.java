/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import android.graphics.Color;

public class ColorRGB 
{
	private double Red=0;	
	private double Green=0;
	private double Blue=0;	
	private double Brightness=0;
	
	public ColorRGB()
	{}
	
	public ColorRGB(int R, int G, int B)
	{
		this.Red = R;
		this.Green = G;
		this.Blue = B;
		this.Brightness = 1;
	}
	
	public ColorRGB(int R, int G, int B, int Brightness)
	{
		this.Red = R;
		this.Green = G;
		this.Blue = B;
		this.Brightness = Brightness;
	}
	
	public double getRed() 
	{
		return Red;
	}
	
	public void setRed(double red) 
	{
		Red = red;
	}
	
	public double getGreen() 
	{
		return Green;
	}
	
	public void setGreen(double green) 
	{
		Green = green;
	}
	
	public double getBlue() 
	{
		return Blue;
	}
	
	public void setBlue(double blue) 
	{
		Blue = blue;
	}
	
	public double getBrightness() 
	{
		return Brightness;
	}
	
	public void setBrightness(double brightness) 
	{
		Brightness = brightness;
	}
	
	public double DistanceToColor(ColorRGB color)
	{
		return ColorRGB.ColourDistance((long)this.Red, (long)this.Green, (long)this.Blue, (long)color.Red, (long)color.Green, (long)color.Blue);
	}
	
	public static double ColourDistance(int color1, int color2)
	{
		return ColorRGB.ColourDistance(Color.red(color1), Color.green(color1), Color.blue(color1), Color.red(color2), Color.green(color2), Color.blue(color2));
	}
	
	public static double ColourDistance(long r1, long g1, long b1, long r2, long g2, long b2)
	{				
		long r = r1 - r2;
		long g = g1 - g2;
		long b = b1 - b2;
		//long rmean = ( r1 + r2) / 2;
		//return Math.sqrt((((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8));
		return Math.sqrt(r*r + g*g + b*b);
	}
}
