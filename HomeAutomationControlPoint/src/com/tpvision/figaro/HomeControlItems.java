/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import android.util.Log;

import com.tpvision.figaro.HomeControlItem.DataChangeListener;
import com.tpvision.figaro.HomeControlItem.DeviceBindingTypeEnum;
import com.tpvision.figaro.HomeControlItem.DeviceTypeEnum;
import com.tpvision.sensormgt.upnpcontrolpoint.model.DataItem;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorCollection;

@Root(name="Nodes")
public class HomeControlItems implements DataChangeListener 
{
	private static final String TAG = "HomeControlItems";
	@ElementList(inline=true)
	public List<HomeControlItem> items = new ArrayList<HomeControlItem>();
	private DataChangeListener mListener;
	
	private static final String SCT_FRIDGE = "urn:upnp-org:smgt-sct:refrigerator:";
	
	public HomeControlItems() {
		
	}
	
	public HomeControlItems(boolean fake) {
		if (fake) addFakeDevices();	
	}
	
	public void setOnDataChangedListener(DataChangeListener listener) {
		mListener = listener;
	}
	
	public void removeDataChangedListener() {
		mListener = null;
	}
	
	@Override
	public void onDataChanged(DataItem dataItem) {
		Log.w(TAG,"NOTIFY DATASET CHANGED");
		if (mListener!=null) mListener.onDataChanged(dataItem);
	}
	
	public Boolean addFakeDevices() {
		
		HomeControlItem homeControlItem = new HomeControlItem("Sensor5", DeviceTypeEnum.LIGHT_LIVING_COLORS, DeviceBindingTypeEnum.SINK, "Hue");
		homeControlItem.setOn(true);
		this.items.add(homeControlItem);
		homeControlItem = new HomeControlItem("Sensor6", DeviceTypeEnum.SWITCH, DeviceBindingTypeEnum.SOURCE, "Switch");
		this.items.add(homeControlItem);
		homeControlItem = new HomeControlItem("Sensor7", DeviceTypeEnum.DIMMER, DeviceBindingTypeEnum.SOURCE, "Dimmer");
		this.items.add(homeControlItem);
		return true;
	}
	
//	public Boolean initWithConnectedDevices(UPnPControlPoint controlpoint, String currentUPnPDevice_UDN)
//	{
//		HomeControlItems homeControlItemsTemp = new HomeControlItems();	    	    	
//	    String nodesXML = controlpoint.getCNNodes(currentUPnPDevice_UDN, CN_Networks_Enum.RF4CENetwork.name());
//	    String nodeInfo;
//    	try
//    	{
//    		Serializer serializer = new Persister();       	
//    		homeControlItemsTemp = serializer.read(HomeControlItems.class, nodesXML);
//    		for (HomeControlItem homeControlItem: homeControlItemsTemp.items)
//    		{    	
//    			nodeInfo = controlpoint.getNodeInfo(currentUPnPDevice_UDN, CN_Networks_Enum.RF4CENetwork.name(), homeControlItem.nodeId);
//    			homeControlItem = serializer.read(HomeControlItem.class, nodeInfo);
//    			homeControlItem.initUPnPActions(controlpoint, currentUPnPDevice_UDN);
//    			homeControlItem.processActions();
//    			this.items.add(homeControlItem);    			
//    		}
//    		return true;
//    	} catch (Exception e) 
//		{}
//    	return false;
//	}
	
	/**
	 * check what type of SensorCollection is added and create the appropriate UI component 
	 * @param sensorCollection
	 */
	public void addItem(SensorCollection sensorCollection) {
		
		Log.d(TAG, "addItem");
		if (sensorCollection!=null) {
			
			Iterator<HomeControlItem> ii = items.iterator();
			boolean found = false;
			while(ii.hasNext()&&!found) {
				HomeControlItem homeControlItem = ii.next();
				found = (homeControlItem.getSensorCollection()!=null) && (homeControlItem.getSensorCollection().equals(sensorCollection));
			}
			
			if (!found) {
				HomeControlItem homeControlItem;
				String sensorCollectionType = sensorCollection.getType();
				Log.d(TAG, "new HomeControl Item sensorCollectionType"+sensorCollection.getType());
				if (sensorCollectionType.startsWith(SCT_FRIDGE)) {
					
					homeControlItem = new HomeControlItemFridge(this, sensorCollection, "Sensor1", sensorCollection.getFriendlyName());
				}
				else {
				Log.d(TAG, "new HomeControl Item"+sensorCollection.getFriendlyName());
				
				if (sensorCollection.getSensorCollectionId().contentEquals("SensorCollection3") || 
						sensorCollection.getSensorCollectionId().contentEquals("SensorCollection4")) {
					homeControlItem = new HomeControlItemEnergy(this, sensorCollection, "Sensor1", DeviceBindingTypeEnum.SINK, sensorCollection.getFriendlyName());
				}
				else {
					homeControlItem = new HomeControlItem(this, sensorCollection, "Sensor1", DeviceTypeEnum.LIGHT, DeviceBindingTypeEnum.SINK, sensorCollection.getFriendlyName());
				}
				}
				this.items.add(homeControlItem);  
			}
		}
	}
}