/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */


package com.tpvision.figaro;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Parameter")
public class ActionParameter 
{
	public enum ParameterIdEnum
	{	
		R,
		G,
		B,
		T
	};
	
	@Attribute(name="id", required=false)
	public ParameterIdEnum id;
	
	@Attribute(name="name", required=false)
	public String name;
	
	@Attribute(name="type", required=false)
	public String type;
	
	@Attribute(name="maxvalue", required=false)
	public double maxValue;
	
	@Attribute(name="minvalue", required=false)
	public double minValue;		
	
	@Attribute(name="currentvalue", required=false)
	public double currentValue;
	
	public ActionParameter(ParameterIdEnum id, String name, double minValue, double maxValue, String type)
	{
		Init(id, name, minValue, maxValue, type);
	}
	
	public ActionParameter()
	{	
		Init(ParameterIdEnum.T, "", 0, 0, "");
	}
	
	private void Init(ParameterIdEnum id, String name, double minValue, double maxValue, String type)
	{
		this.id = id;
		this.name = name;
		this.maxValue = maxValue;
		this.minValue = minValue;
		this.type = type;
		this.currentValue = 0;
	}
}
/*<Parameter id="R" name="Red" type="xs:unsignedShort" minvalue="0" maxvalue="4094"/>*/
