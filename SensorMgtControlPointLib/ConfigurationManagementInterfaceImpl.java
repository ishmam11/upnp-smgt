package com.tpvision.sensormgt.upnpcontrolpoint.clinginterface;

import org.fourthline.cling.android.AndroidUpnpService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.meta.Action;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.types.UnsignedIntegerFourBytes;



import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetAttributesCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetConfigurationUpdateCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetCurrentConfigurationVersionCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetInstancesCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetSupportedDatamodelsCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetSupportedParametersCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.GetValuesCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.SetAttributesCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.ConfigurationMgtCallbacks.SetValuesCallback;
import com.tpvision.sensormgt.upnpcontrolpoint.upnpinterface.ConfigurationManagementCPInterface;


public class ConfigurationManagementInterfaceImpl implements ConfigurationManagementCPInterface {

	
	private static final String ACTION_GETSUPPORTEDDATAMODELS = "GetSupportedDataModels";
	private static final String ACTION_GETSUPPORTEDPARAMETERS = "GetSupportedParameters";
	private static final String ACTION_GETINSTANCES = "GetInstances";
	private static final String ACTION_GETVALUES = "GetValues";
	private static final String ACTION_SETVALUES = "SetValues";
	private static final String ACTION_GETATTRIBUTES = "GetAttributes";
	private static final String ACTION_SETATTRIBUTES = "SetAttributes";
	private static final String ACTION_GETCONFIGURATIONUPD = "getConfigurationUpdate";	
	
	
	private Service<?, ?> mService;
	private AndroidUpnpService mAndroidUpnpService;
	private ConfigurationMgtCallbacks mConfigurationMgtCallbacks;
	private SerialActionExecutor mSerialExecutor;
	
	public ConfigurationManagementInterfaceImpl(SerialActionExecutor executor, AndroidUpnpService androidUpnpService, Service<?, ?> confMgtService) {
		mAndroidUpnpService = androidUpnpService;
		mService = confMgtService;
		mConfigurationMgtCallbacks = new ConfigurationMgtCallbacks();
		mSerialExecutor = executor;
	}
	
	synchronized public void getSupportedDatamodels(GetSupportedDatamodels callback) {
		Action lAction = mService.getAction(ACTION_GETSUPPORTEDDATAMODELS);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			GetSupportedDatamodelsCallback lCallback = mConfigurationMgtCallbacks.new GetSupportedDatamodelsCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	synchronized public void getSupportedParameters(String startingNodePath, int searchDepth, GetSupportedParameters callback) {
		Action lAction = mService.getAction(ACTION_GETSUPPORTEDPARAMETERS);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("StartingNode", startingNodePath);
			lInvocation.setInput("SearchDepth", new UnsignedIntegerFourBytes(searchDepth));

			GetSupportedParametersCallback lCallback = mConfigurationMgtCallbacks.new GetSupportedParametersCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	synchronized public void getInstances(String startingNodePath, int searchDepth, GetInstances callback) {
		Action lAction = mService.getAction(ACTION_GETINSTANCES);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("StartingNode", startingNodePath);
			lInvocation.setInput("SearchDepth", new UnsignedIntegerFourBytes(searchDepth));

			GetInstancesCallback lCallback = mConfigurationMgtCallbacks.new GetInstancesCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void getValues(String parameters, GetValues callback) {
		Action lAction = mService.getAction(ACTION_GETVALUES);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("Parameters", parameters);

			GetValuesCallback lCallback = mConfigurationMgtCallbacks.new GetValuesCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void setValues(String parametersValues, SetValues callback) {
		Action lAction = mService.getAction(ACTION_SETVALUES);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("ParameterValueList", parametersValues);

			SetValuesCallback lCallback = mConfigurationMgtCallbacks.new SetValuesCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void getAttributes(String attributePaths, GetAttributes callback) {
		Action lAction = mService.getAction(ACTION_GETATTRIBUTES);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("Parameters", attributePaths);

			GetAttributesCallback lCallback = mConfigurationMgtCallbacks.new GetAttributesCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void setAttributes(String nodeAttrValueList, SetAttributes callback) {
		Action lAction = mService.getAction(ACTION_SETATTRIBUTES);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			lInvocation.setInput("NodeAttributeValueList", nodeAttrValueList);

			SetAttributesCallback lCallback = mConfigurationMgtCallbacks.new SetAttributesCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void getConfigurationUpdate(GetConfigurationUpdate callback) {
		Action lAction = mService.getAction(ACTION_GETCONFIGURATIONUPD);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			GetConfigurationUpdateCallback lCallback = mConfigurationMgtCallbacks.new GetConfigurationUpdateCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
	}

	@Override
	synchronized public void getCurrentConfigurationVersion(
			GetCurrentConfigurationVersion callback) {
		Action lAction = mService.getAction(ACTION_GETCONFIGURATIONUPD);

		if (lAction != null) {
			ActionInvocation lInvocation = new ActionInvocation(lAction);

			GetCurrentConfigurationVersionCallback lCallback = mConfigurationMgtCallbacks.new GetCurrentConfigurationVersionCallback(callback, lInvocation, mAndroidUpnpService.getControlPoint());
			mSerialExecutor.execute(lCallback);
			//mAndroidUpnpService.getControlPoint().execute(lCallback);
		}
		
	}

	
	
}